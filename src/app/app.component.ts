import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'festrr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'festrr';

  constructor() {
  }

  ngOnInit() {
  }
}
